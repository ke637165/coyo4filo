import {CoyoPage} from './CoyoPage';
import {Timeline} from './timeline/Timeline';

export class HomePage extends CoyoPage {

    public personalTimeline: Timeline = new Timeline();

    constructor() {
        super('/home');
    }
}
