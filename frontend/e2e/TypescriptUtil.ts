import * as _ from 'underscore';

/**
 * Multi-inheritance / mixins for classes.
 * @see https://www.typescriptlang.org/docs/handbook/mixins.html
 *
 * The derived class can override a function by declaring the same function with a preceding underscore.
 * The first argument will be the super function.
 *
 * @param derivedCtor The constructor of the object that should receive the mixin functions.
 * @param {any[]} baseCtors The constructors of the objects the mixin functions should be taken from
 */
export function applyMixins(derivedCtor: any, baseCtors: any[]) {
    const fn = (property: any, name: string): any => {
        const overridden = derivedCtor.prototype['_' + name];
        return _.isFunction(property) && _.isFunction(overridden)
            ? function () { // do NOT use fat arrow ()=>{} syntax here
                const args = Array.prototype.slice.call(arguments);
                args.unshift(property.bind(this)); // add super function as first argument
                overridden.apply(this, args);
            }
            : property;
    };
    baseCtors.forEach((baseCtor) => {
        Object.getOwnPropertyNames(baseCtor.prototype).forEach((name) => {
            derivedCtor.prototype[name] = fn(baseCtor.prototype[name], name);
        });
    });
}
