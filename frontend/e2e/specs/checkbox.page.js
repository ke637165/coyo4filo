(function () {
  'use strict';

  function Checkbox(element) {
    var api = this;
    api.click = function () {
      return element.$('.coyo-checkbox').click();
    };
    api.isChecked = function () {
      return element.$('.coyo-checkbox').getAttribute('class').then(function (clazz) {
        return /checked/.test(clazz);
      });
    };
    api.set = function (checked) {
      return api.isChecked().then(function (isChecked) {
        var shouldBeChecked = (checked === undefined) || !!checked; //eslint-disable-line
        if (isChecked !== shouldBeChecked) {
          return api.click();
        } else {
          return protractor.promise.defer().fulfill();
        }
      });
    };
    api.reset = function () {
      return api.set(false);
    };
  }

  module.exports = Checkbox;

})();
