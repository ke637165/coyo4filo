(function () {
  'use strict';

  var App = require('./../app.page');
  var WikiPage = require('./wiki.page');
  var WorkspaceDetail = require('./../../workspaces/workspace-details.page.js');
  var component = require('./../../../components.page');

  var app = new App();
  var wikiPage = new WikiPage();
  var workspaceDetail = new WorkspaceDetail();

  function WikiHelper() {
    var api = this;

    api.createEmptyWikiApp = createEmptyWikiApp;
    api.createWikiArticle = createWikiArticle;
    api.getFirstVisibleElement = getFirstVisibleElement;
    api.moveArticles = moveArticles;
    api.hoverElement = hoverElement;

    function createEmptyWikiApp() {
      var appName = 'Wiki';
      workspaceDetail.options.addApp.click();
      app.app.wiki.click();
      wikiPage.setting.appName.clear();
      wikiPage.setting.appName.sendKeys(appName);

      expect(wikiPage.setting.active.isChecked()).toBeTruthy();

      component.modals.confirm.confirmButton.click();

      expect(wikiPage.articleList.empty.isPresent()).toBeTruthy();
    }

    function createWikiArticle(title, text) {
      expect(getFirstVisibleElement(wikiPage.articleList.create).isPresent()).toBeTruthy();
      getFirstVisibleElement(wikiPage.articleList.create).click();

      expect(wikiPage.article.create.header.isPresent()).toBeTruthy();
      expect(wikiPage.article.create.titleInput.isPresent()).toBeTruthy();
      expect(wikiPage.article.create.editorInput.isPresent()).toBeTruthy();

      wikiPage.article.create.titleInput.sendKeys(title);
      wikiPage.article.create.editorInput.sendKeys(text);
      wikiPage.article.create.saveButton.click();
    }

    function getFirstVisibleElement(selector) {
      var allElementsOfSelector = element.all(by.css(selector.locator().value));
      return allElementsOfSelector.filter(function (e) {
        return e.isDisplayed().then(function (displayedElement) {
          return displayedElement;
        });
      }).first();
    }


    /**
     * Reorders questions by dragging and dropping.
     */
    function moveArticles(articleToMove, articleToMoveTo) {
      browser.actions().dragAndDrop(articleToMove, articleToMoveTo).perform();
      browser.sleep(1000);    // wait for animation

    }

    function hoverElement(selector) {
      browser.actions().mouseMove(selector).perform();
    }

  }

  module.exports = WikiHelper;
})();
