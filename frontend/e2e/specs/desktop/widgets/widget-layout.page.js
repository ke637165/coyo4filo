(function () {
  'use strict';

  var WidgetSlot = require('./widget-slot.page.js');

  function WidgetLayout(name) {
    var api = this;
    var widgetLayout = element(by.css('.widget-layout'));

    api.getSlot = function () {
      return new WidgetSlot(name);
    };

    api.addButton = widgetLayout.$('.widget-layout-row-create .zmdi-plus');
    api.removeButton = widgetLayout.$('.widget-layout-row-delete .zmdi-minus');
  }

  module.exports = WidgetLayout;

})();
