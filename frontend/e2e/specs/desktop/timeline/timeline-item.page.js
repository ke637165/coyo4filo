(function () {
  'use strict';

  var SelectSender = require('../commons/select-sender.page'),
      Comments = require('../commons/comments.page'),
      BtnLikes = require('../commons/btn-likes.page'),
      ContextMenu = require('../../context-menu.page'),
      components = require('../../components.page');

  function TimelineItem(timelineItemElement) {
    var api = this,
        timelineItemFooter = timelineItemElement.$('.timeline-item-footer');

    api.element = timelineItemElement;
    api.authorName = api.element.$('.timeline-item-author-name');
    api.message = api.element.$('.timeline-item-message');
    api.likeBtn = BtnLikes.create(timelineItemFooter);
    api.selectSender = SelectSender.create(timelineItemFooter);
    api.comments = Comments.create(api.element);
    api.embeddedVideo = api.element.$('.video-preview');
    api.menu = new ContextMenu(api.element);
    api.stickyRibbon = api.element.$('.ribbon-sticky');
    api.stickyReadRibbon = api.element.$('.ribbon-sticky-read');
    api.unstickyModal = components.modals.confirm;

    api.markAsRead = function () {
      api.stickyReadRibbon.click();
    };
  }

  TimelineItem.getAllElements = function (context) {
    return context.$$('.timeline-item');
  };

  TimelineItem.create = function (context, index) {
    var element = TimelineItem.getAllElements(context).get(index);
    return new TimelineItem(element);
  };

  module.exports = TimelineItem;

})();
