(function () {
  'use strict';

  function UserChooser(chooserElement) {
    var api = this;
    api.click = function () {
      chooserElement.$('.user-chooser').click();
    };

    api.close = function () {
      $('.user-chooser .modal-close').click();
    };

    api.selectedTab = {
      list: $$('.tab-content .active .user-chooser-item')
    };

    api.userTab = {
      click: function () {
        $('li[select="vm.searchUsers(vm.users.term)"] a').click();
      },
      search: function (searchTerm) {
        $('.tab-content .active').element(by.model('$ctrl.searchTerm')).sendKeys(searchTerm);
      },
      list: $$('.tab-content .active .user-chooser-item')
    };
  }

  module.exports = UserChooser;

})();
